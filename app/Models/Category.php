<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    use HasFactory;

    protected $fillable = [  //разрешаем Классу менять эти поля в базе данных
        'name',
        'price',
        'description',
        'sort_order',
        'slug',
        'title',
        'publish',
        'created_at',
        'updated_at',
    ];




    public function products()
    {
        return $this->belongsToMany(Product::class);
    }




}
