<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [  //разрешаем Классу менять эти поля в базе данных
        'user_id',
        'phone',
        'created_at',
        'updated_at',
    ];


}
