<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Article extends Model
{
    use HasFactory;

    protected $fillable = [  //разрешаем Классу менять эти поля в базе данных
        'name',
        'description',
        'slug',
        'title',
        'created_at',
        'updated_at',
    ];
}
