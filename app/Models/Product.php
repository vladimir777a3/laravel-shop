<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [  //разрешаем Классу менять эти поля в базе данных
        'name',
        'vendor_code',
        'price',
        'description',
        'sort_order',
        'manufacturer_id',
        'stock_status_id',
        'main_image',
        'slug',
        'title',
        'publish',
        'created_at',
        'updated_at',

    ];


    public function category()
    {
        return $this->belongsToMany(Category::class);
    }



}


