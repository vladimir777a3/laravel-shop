<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class SingleProductController extends Controller
{
    public function index(Request $request, $slag)
    {
        $product = Product::where('slug', $slag)->first();
        $title = $product->title;
        $name = $product->name;
        $price = $product->price;



//Определить последнюю категорию и передпть во Вьюшку в Хлебные крошки
        return view('single-product', ['title' => $title, 'name' => $name, 'price' => $price]);
    }
}
