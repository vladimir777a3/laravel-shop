<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $users = User::query();
        $id = $request->get('id');
        $email = $request->get('email');
        $paginate = $request->get('paginate',20);
        if ($id) {
            $users->where('id', $id);
        }
        if ($email) {
            $users->where('email', 'like', '%' . $email . '%');
        }


        return view('admin.users.index', [
            'users' => $users->orderBy('phone')->paginate($paginate ),
            'id' => $id,
            'email' => $email,
        ]);
    }
    public function ajax(Request $request)
    {
        $users = User::query();
        $id = $request->get('id');
        $email = $request->get('email');
        $paginate = $request->get('paginate',20);
        if ($id) {
            $users->where('id', $id);
        }
        if ($email) {
            $users->where('email', 'like', '%' . $email . '%');
        }


        return response()->json($users->orderBy('phone')->paginate($paginate ));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();

        $request->validate([
            'email'=>'email|required|max:200',
        ]);
        
        $user->fill(  
            $request->only(['name',        'email',        'phone'])
        );
        $user->save();

        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.users.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,User $user)
    {


       
        return view('admin.users.edit',[
            'user'=> $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $request->validate([
            'email'=>'email|required|max:200',
        ]);
        
        $user->fill(  
            $request->only(['name',        'email',        'phone'])
        );
        $user->save();

        return redirect(route('admin.user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( User $user)
    {

        $user->delete();

        return redirect(route('admin.user.index'));
    }
}
