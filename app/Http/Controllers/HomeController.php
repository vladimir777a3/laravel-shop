<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /* Страница не показывается без авторизации
    public function __construct()
    {
        $this->middleware('auth');
    }
*/
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      /*  $products = \DB::table('oc_product')->get();  //заливка инфы со старой базы
        foreach ($products as $product) {
            dd($product);

        }
*/
        return view('home');
    }



}
