<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\User;


class TestController extends Controller
{
    public function index()
    {





        $id = 1;
        $n = 2;
        return view('test', ['name' => 'Taylor', 'id' => $id, 'n' => $n]);
    }
}


/*Взять данные из старой таблицы и обновить эти же id в текущей таблице:

use App\Models\Product;
use Illuminate\Support\Facades\DB;

public function index()
    {
        $old_products = DB::table('oc_product_description')->get();

        foreach ($old_products as $single_old_product) {

            $id_old = $single_old_product->product_id;

            $myProduct = Product::where('id', $id_old)->first();

            $myProduct->name = $single_old_product->name;
            $myProduct->description = $single_old_product->description;
            $myProduct->title = $single_old_product->meta_title;
            $myProduct->save();

            echo($myProduct->id . ' - ok<br>');

        }
        dd('foreach отработал - ок');
    }*/


/*
обрезать строку в текущей базе
 $products = DB::table('products')->get();

        $n = 1;
        foreach ($products as $single_product) {

            $image_old = $single_product->main_image;
            $image_new = mb_strimwidth($image_old, 8, 999);

            $myProduct = Product::where('id', $single_product->id)->first();

            $myProduct->main_image = $image_new;

            $myProduct->save();

            echo($n.' - '.$myProduct->main_image . ' - ok<br>');
            $n++;
        }
        dd('foreach отработал - ок');

*/

/*Переливка данных из одной таблицы в другую
$products = DB::table('products')->get();

        $n = 1;
        foreach ($products as $single_product) {

            $image_old = $single_product->main_image;
            $image_new = mb_strimwidth($image_old, 8, 999);

            $myProduct = Product::where('id', $single_product->id)->first();

            $myProduct->main_image = $image_new;

            $myProduct->save();

            echo($n.' - '.$myProduct->main_image . ' - ok<br>');
            $n++;
        }
        dd('foreach отработал - ок');
*/
