<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Article;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|Здесь вы можете зарегистрировать веб-маршруты для своего приложения.
Эти маршруты загружаются RouteServiceProvider внутри группы,
которая содержит группу middleware «web».
Теперь создайте что-нибудь отличное!
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');

//Route::get('/article', [App\Http\Controllers\ArticleController::class, 'index']);

//Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('category');

Route::get('/contact', [App\Http\Controllers\ContactController::class, 'index']);

Route::get('/cart', [App\Http\Controllers\CartController::class, 'index']);

//Route::get('/test/{user}', [App\Http\Controllers\TestController::class, 'index']);
Route::get('/test', [App\Http\Controllers\TestController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

$request = request();
$url = $request->path();

// if (Product::where('slug', $url)->first()) {
//     Route::get('/{slug}', [App\Http\Controllers\SingleProductController::class, 'index']);
// }

// if (Category::where('slug', $url)->first()) {
//     Route::get('/{slug}', [App\Http\Controllers\CategoryController::class, 'index']);
// }

// if (Article::where('slug', $url)->first()) {
//     Route::get('/{slug}', [App\Http\Controllers\ArticleController::class, 'index']);
// }




/*
$slugs = DB::table('products')->get('slug');

foreach ($slugs as $slug) {

    Route::get('/{' . $slug->slug . '}', [App\Http\Controllers\SingleProductController::class, 'index'])->name('single-product');

}
"

$slugs = DB::table('categories')->get('slug');
foreach ($slugs as $slug) {
    Route::get('/{slug}', [App\Http\Controllers\CategoryController::class, 'index'])->name('category');
}
*/
//------------------------------------------ADMIN----------------------------
Route::prefix('admin')->name('admin.')->middleware('admin')->group(function () {

    
    Route::get('/user/ajax', [ App\Http\Controllers\Admin\UserController::class,'ajax' ]);
    Route::resource('user', App\Http\Controllers\Admin\UserController::class);
   

});



























