@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <form id="search_form">
                    <input id="id" name="id" type="text" placeholder="id" value="{{request()->get('id')}}" />
                    <input id="email" name="email" type="text" placeholder="email" value="{{request()->get('email')}}" />
                    <select name="paginate">
                        <option> Записей на странице {{request()->get('paginate')}} </option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>

                    </select>
                    <input type="submit">
                </form>

                @if($users->count()>0)
                <table border="1">
                    @foreach($users as $user)
                    <tr>
                        <td><a href="{{ route('admin.user.edit',['user'=>$user->id]) }}">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>

                            <form method="POST" action="{{ route('admin.user.destroy',['user'=>$user->id]) }}">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">

                                <input type="submit" value="delete {{ $user->name }}" />

                            </form>

                        </td>
                    </tr>
                    @endforeach
                </table>
                {{$users->links()}}
                @else
                no records
                @endif
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    $('#search_form').bind('submit', function(e) {
        e.preventDefault();
        
        var id  = $('#id').val();
        var email  = $('#email').val();
        $('table tr').remove();
        $.ajax({
            url: "http://127.0.0.1:8000/admin/user/ajax",
            data: {id :id,email:email }
        }).done(function(data) {
            //console.log(data);
            $(data.data).each(function(i, item) {
                // console.log(i, item);

                $('table').append('<tr>' 

                    +'<td>' + item.name + '</td>' +
                    '<td>' + item.email + '</td>' +
                    '<td>' + item.phone + '</td>' +
                    '</tr>');
            });
        });

    });
</script>
@endsection