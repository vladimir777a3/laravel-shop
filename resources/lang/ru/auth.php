<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines - Строки языка аутентификации
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    Следующие языковые строки используются во время аутентификации для различных
| сообщения, которые нам нужно показать пользователю. Вы можете изменить
| эти языковые строки в соответствии с требованиями вашего приложения.
    */


    'failed'   => 'Неверное имя пользователя или пароль.',
    'password' => 'Неверный пароль.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',
];
