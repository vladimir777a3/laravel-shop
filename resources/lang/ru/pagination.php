<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines - Строки языка пагинации
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    Следующие языковые строки используются библиотекой paginator для построения простых ссылок на страницы.
    Вы можете изменить их на все, что хотите, чтобы настроить представления,
    чтобы они лучше соответствовали вашему приложению.
    |
    */

    'next'     => 'Вперёд &raquo;',
    'previous' => '&laquo; Назад',

];
