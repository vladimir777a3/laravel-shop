<?php



    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines - Строки языка аутентификации
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    Следующие языковые строки используются во время аутентификации для различных
| сообщения, которые нам нужно показать пользователю. Вы можете изменить
| эти языковые строки в соответствии с требованиями вашего приложения.
    */

return [
    'failed'   => 'Вказані облікові дані не збігаються з нашими записами.',
    'password' => 'Наданий невірний пароль.',
    'throttle' => 'Забагато спроб входу. Будь ласка, спробуйте ще раз, через :seconds секунд.',
];
