<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable($value = true)->comment('Може быть Null, если заказ сделан без регистрации');
            $table->string('phone')->comment('телефон');
            $table->dateTime('created_at', $precision = 0)->comment('Дата создания');
            $table->dateTime('updated_at', $precision = 0)->comment('Дата изменения');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
