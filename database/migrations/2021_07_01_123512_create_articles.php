<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Название статьи для людей');

            $table->string('title')->comment('title страницы для поисковика');

            $table->longText('description')->comment('Описание категории');
            $table->string('slug')->comment('Человекопонятный URL');
            $table->dateTime('created_at', $precision = 0)->comment('Дата создания');
            $table->dateTime('updated_at', $precision = 0)->comment('Дата изменения');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
