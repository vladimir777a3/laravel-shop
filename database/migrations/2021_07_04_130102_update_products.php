<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('vendor_code')->nullable($value = true)->comment('Артикул')->change();  //change Метод позволяет изменять тип и атрибуты существующих столбцов.
            $table->string('name')->nullable($value = true)->comment('Название товара')->change();
            $table->decimal('price')->nullable($value = true)->comment('Цена, грн с НДС')->change();
            $table->longText('description')->nullable($value = true)->comment('Описание товара')->change();
            $table->integer('sort_order')->nullable($value = true)->comment('Порядок сортировки')->change();
            $table->integer('manufacturer_id')->nullable($value = true)->comment('id производителя')->change();
            $table->dateTime('created_at')->nullable($value = true)->comment('Дата создания')->change();
            $table->dateTime('updated_at')->nullable($value = true)->comment('Дата обновления')->change();
            $table->integer('stock_status_id')->nullable($value = true)->comment('Статус склада или срок поставки')->change();
            $table->string('main_image')->nullable($value = true)->comment('Главная картинка')->change();
            $table->string('slug')->nullable($value = true)->change();
            $table->string('title')->nullable($value = true)->change();
            $table->integer('publish')->nullable($value = true)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('vendor_code')->nullable($value = false)->comment('')->change();
            $table->string('name')->nullable($value = false)->comment('')->change();
            $table->decimal('price')->nullable($value = false)->comment('')->change();
            $table->longText('description')->nullable($value = false)->comment('')->change();
            $table->integer('sort_order')->nullable($value = false)->comment('')->change();
            $table->integer('manufacturer_id')->nullable($value = false)->comment('')->change();
            $table->dateTime('created_at')->nullable($value = false)->comment('')->change();
            $table->dateTime('updated_at')->nullable($value = false)->comment('')->change();
            $table->integer('stock_status_id')->nullable($value = false)->comment('')->change();
            $table->string('main_image')->nullable($value = false)->comment('')->change();
            $table->string('slug')->nullable($value = false)->change();
            $table->string('title')->nullable($value = false)->change();
            $table->integer('publish')->nullable($value = false)->change();

        });
    }
}
