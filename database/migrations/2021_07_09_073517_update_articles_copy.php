<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateArticlesCopy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('name')->nullable($value = true)->change();  //change Метод позволяет изменять тип и атрибуты существующих столбцов.
            $table->string('title')->nullable($value = true)->change();
            $table->decimal('description')->nullable($value = true)->change();
            $table->longText('slug')->nullable($value = true)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('name')->nullable($value = false)->change();  //change Метод позволяет изменять тип и атрибуты существующих столбцов.
            $table->string('title')->nullable($value = false)->change();
            $table->decimal('description')->nullable($value = false)->change();
            $table->longText('slug')->nullable($value = false)->change();

        });
    }
}
