<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id()->comment('id категории');
            $table->string('name')->comment('Название категории для людей');
            $table->string('title')->comment('title страницы для поисковика');

            $table->longText('description')->comment('Описание категории');
            $table->string('slug')->comment('Человекопонятный URL');

            $table->integer('sort_order')->comment('Порядок сортировки');
            $table->integer('publish')->comment('1-отображать или 0-нет');

            $table->dateTime('created_at', $precision = 0)->comment('Дата создания');
            $table->dateTime('updated_at', $precision = 0)->comment('Дата изменения');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
